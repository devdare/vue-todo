import * as firebase from "firebase";

//Firebase config
const firebaseConfig = {
	apiKey: "AIzaSyAUNMQOG-Eu-TofMgJQUY4e2XzuXl5sSn4",
	authDomain: "vue-todo-7c940.firebaseapp.com",
	databaseURL: "https://vue-todo-7c940.firebaseio.com",
	projectId: "vue-todo-7c940",
	storageBucket: "vue-todo-7c940.appspot.com",
	messagingSenderId: "598185047001",
	appId: "1:598185047001:web:2f9278d9ea45a91c171fc8",
	measurementId: "G-C4JP8KNN0Z"
};
//Initialize firebase
const app = firebase.initializeApp(firebaseConfig);
export const dbs = app.database();
export const db = app.firestore();
export const todosList = db.collection("todos");
// const Todos = firebase.database().ref("/todos");
